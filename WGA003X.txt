       IDENTIFICATION DIVISION.
       PROGRAM-ID.    WGA003X.
      ******************************************************************
      *    TITLE ........... CALL VALINDEX WEB SERVICE PROGRAM WGA003
      *                      VALIDATE ACCOUNT INDEX/IFOAPAL CREATION
      *    LANGUAGE ........ COBOL Z/OS, CICS
      *    AUTHOR .......... H. VANDERWEIT
      *    DATE-WRITTEN .... JULY, 2007
      *
      *                      DESCRIPTION
      *                      -----------
      *    THIS IS A WEB SERVICE "STUB" PROGRAM THAT RECEIVES A SERVICE
      *    REQUEST (IN THE COMMAREA) FROM THE WEBSPHERE CLIENT,
      *    EDITS THE COMMAREA ID, VERSION, AND LENGTH, AND THEN CALLS
      *    MAINFRAME PROGRAM WGA003 TO PERFORM THE ASSOCIATED SERVICE.
      *    THE MAINFRAME PROGRAM HANDLES ALL BUSINESS LOGIC AND RETURNS
      *    RESULTS IN THE COMMAREA, WHICH IS SIMPLY PASSED BACK TO THE
      *    WEBSPHERE CLIENT BY THIS PROGRAM.
      *
      *    THIS "STUB" PROGRAM IS NECESSARY BECAUSE PROGRAM WGA003 IS
      *    COMPILED AS A DUALCOB PROGRAM (CICS AND BATCH), WHICH MEANS
      *    IT CANNOT CONTAIN "EXEC CICS" COMMANDS AS USED IN THIS
      *    PROGRAM FOR CICS NAVIGATION AND ERROR HANDLING.
      *
      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
      *    MODIFIED BY .....
      *    DATE MODIFIED ...
      *    MODIFICATION ....
      ******************************************************************
      /
       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01  FILLER                      PIC X(36)       VALUE
           'WGA003X WORKING-STORAGE BEGINS HERE '.

       01  WS-VALINDEX-PROGRAM         PIC X(08)       VALUE 'WGA003'.
       01  WS-DUMMY-COMMAREA           PIC X(01)       VALUE SPACES.

       01  WS-MISCELLANEOUS.
           05  WS-SQL-PARAGRAPH.
               10  FILLER              PIC X(04)       VALUE 'I/O#'.
               10  WS-SQL-NBR          PIC 9(04)       VALUE ZEROES.
               10  FILLER              PIC X(02)       VALUE SPACES.
               10  WS-PARAGRAPH        PIC X(30)       VALUE SPACES.

       01  WSLG-USING-LIST.
           05  WSLG-CONSOLE-MSG-IND    PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-ID             PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-DATE           PIC X(10)       VALUE SPACES.
           05  WSLG-MSG-TIME           PIC X(08)       VALUE SPACES.
           05  WSLG-PROGRAM-NAME       PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-SEQ-NUM        PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-TEXT           PIC X(80)       VALUE SPACES.

       01  WSLG-DIAGNOSTIC-AREA.
           05  FILLER                  PIC X(05)       VALUE 'PARA:'.
           05  WSLG-PARAGRAPH          PIC X(30)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'TASK:'.
           05  WSLG-CICS-TASK-NUMBER   PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  WSLG-DIAGNOSTIC-INFO    PIC X(30)       VALUE SPACES.

       01  WSLG-MISC-AREA.
           05  WSLG-PROGRAM-UIS000W    PIC X(08)       VALUE 'UIS000W '.
           05  WSLG-CICS-RESP          PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-CICS-RESP2         PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-EIBCALEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-CHANLLEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-DB2-STATEMENT      PIC X(08)       VALUE SPACES.
           05  WSLG-DB2-TABLE          PIC X(16)       VALUE SPACES.
           05  WSLG-SQL-NBR            PIC 9(04)       VALUE ZEROES.
           05  WSLG-SQL-CODE           PIC +++++9      VALUE ZEROES.

       01  WSLG-MESSAGE-AREA.
           05  WSLG-SUCCESSFUL-MSG-ID  PIC X(08)       VALUE 'WFGA000I'.
           05  WSLG-SUCCESSFUL-MSG-TEXT
                                       PIC X(40)       VALUE
               'The request was successfully completed. '.

           05  WSLG-CHNL-ID-MSG-ID     PIC X(08)       VALUE 'WFGA001E'.
           05  WSLG-CHNL-ID-MSG-TEXT   PIC X(80)       VALUE
               'The received channel ID value is unrecognized.'.

           05  WSLG-CHNL-VERS-MSG-ID   PIC X(08)       VALUE 'WFGA002E'.
           05  WSLG-CHNL-VERS-MSG-TEXT PIC X(80)       VALUE
               'The received channel version value is unrecognized.'.

           05  WSLG-CHNL-LGTH-MSG-ID   PIC X(08)       VALUE 'WFGA003E'.
           05  WSLG-CHNL-LGTH-MSG-TEXT PIC X(80)       VALUE
               'The received channel length is not equal to the defined
      -        'channel version length. '.

           05  WSLG-FATAL-DB2-MSG-ID   PIC X(08)       VALUE 'WFGA004S'.
           05  WSLG-FATAL-DB2-MSG-TEXT.
               10  FILLER              PIC X(49)       VALUE
                   'DB2 returned a severe SQL error on an attempt to '.
               10  WSLG-FATAL-DB2-MSG-FIELDS.
                   15  WSLG-FATAL-DB2-STATEMENT
                                       PIC X(08)       VALUE SPACES.
                   15  FILLER          PIC X(06)       VALUE 'table '.
                   15  WSLG-FATAL-DB2-TABLE
                                       PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *01  VALINDEX-PARM-AREA.                                         *
      *    PARAMETER AREA USED BY DUALCOB/WEB SUBROUTINE WGA003        *
      *    TO VALIDATE ACCOUNT INDEX/IFOAPAL DISTRIBUTION.             *
      ******************************************************************
           COPY WGA003.
      /
       LINKAGE SECTION.
      /
       01  DFHCOMMAREA.
           05  COMM-CHANNEL-HEADER.
               10  COMM-CHANNEL-ID     PIC X(08).
                   88  COMM-CHANNEL-ID-OK              VALUE 'VALINDEX'.
               10  COMM-VERS-RESP-CD   PIC X(08).
                   88  COMM-VERSION-01-00-00           VALUE '01.00.00'.
           05  COMM-VALINDEX-INPUT-PARMS
                                       PIC X(1000).

       PROCEDURE DIVISION.

      /
       A0000-MAINLINE-ROUTINE.
      ******************************************************************
      *    PERFORM ROUTINE TO EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      *    CALL THE VALINDEX MAINFRAME PROGRAM (DUALCOB) BY PASSING A
      *    DUMMY COMMAREA FOLLOWED BY THE WEB-PASSED COMMAREA.
      *    IF A FATAL ERROR CONDITION IS RETURNED, LOG THE FATAL DB2
      *    ERROR VIA UIS000W AND ABEND THE TASK WITH A 'WSQL' DUMP CODE.
      *    RETURN CONTROL TO THE WEB APPLICATION.
      ******************************************************************

           MOVE 'A0000-MAINLINE-ROUTINE'
                                       TO WSLG-PARAGRAPH.

           PERFORM A0500-EDIT-COMMAREA-VERSION.

           MOVE DFHCOMMAREA            TO VALINDEX-PARM-AREA.
           MOVE 'WEB'                  TO VALINDEX-SOURCE-SYSTEM.
           INITIALIZE                     VALINDEX-RETURN-STATUS-PARMS.

           CALL WS-VALINDEX-PROGRAM USING DFHEIBLK
                                          WS-DUMMY-COMMAREA
                                          VALINDEX-PARM-AREA.

           IF VALINDEX-RETURN-STATUS-CODE = 'F'
               MOVE VALINDEX-RETURN-MSG-FIELD (1)
                                       TO WSLG-FATAL-DB2-MSG-FIELDS
               MOVE WSLG-FATAL-DB2-MSG-TEXT
                                       TO WSLG-MSG-TEXT
               MOVE WSLG-FATAL-DB2-MSG-ID
                                       TO WSLG-MSG-ID
               MOVE 'Y'                TO WSLG-CONSOLE-MSG-IND
               MOVE VALINDEX-RETURN-MSG-TEXT (1)
                                       TO WS-SQL-PARAGRAPH
               MOVE WS-SQL-NBR         TO WSLG-SQL-NBR
               MOVE WS-PARAGRAPH       TO WSLG-PARAGRAPH
               MOVE VALINDEX-RETURN-MSG-NUM (1)
                                       TO WSLG-SQL-CODE
               MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
               STRING 'SQL#: '            WSLG-SQL-NBR
                      '  SQLCODE: '       WSLG-SQL-CODE
                   DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                     INTO WSLG-DIAGNOSTIC-INFO
               PERFORM Z9998-LOG-ERROR-VIA-UIS000W

               EXEC CICS ABEND
                    ABCODE('WSQL')
               END-EXEC
           END-IF.

           GO TO Z9999-RETURN-TO-CALLING-PGM.
      /
       A0500-EDIT-COMMAREA-VERSION.
      ******************************************************************
      *    EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      ******************************************************************

           MOVE 'A0500-EDIT-COMMAREA-VERSION'
                                       TO WSLG-PARAGRAPH.
           INSPECT COMM-CHANNEL-ID
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           INITIALIZE                     VALINDEX-PARM-AREA.
           MOVE COMM-CHANNEL-HEADER    TO VALINDEX-CHANNEL-HEADER.

           IF COMM-CHANNEL-ID-OK
               EVALUATE TRUE
               WHEN COMM-VERSION-01-00-00

                   IF EIBCALEN = LENGTH OF VALINDEX-PARM-AREA
                       NEXT SENTENCE
                   ELSE
                       SET VALINDEX-RESPONSE-ERROR
                                       TO TRUE
                       MOVE 'E'        TO VALINDEX-RETURN-STATUS-CODE
                       MOVE  1         TO VALINDEX-RETURN-MSG-COUNT
                       MOVE 'A05001'   TO VALINDEX-RETURN-MSG-NUM (1)
                       MOVE 'CHANNEL-ID'
                                       TO VALINDEX-RETURN-MSG-FIELD (1)
                       MOVE 'The channel length is invalid           '
                                       TO VALINDEX-RETURN-MSG-TEXT (1)
                       MOVE WSLG-CHNL-LGTH-MSG-ID
                                       TO WSLG-MSG-ID
                       MOVE WSLG-CHNL-LGTH-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                       MOVE 'N'        TO WSLG-CONSOLE-MSG-IND
                       MOVE SPACES     TO WSLG-DIAGNOSTIC-INFO
                       MOVE EIBCALEN   TO WSLG-EIBCALEN-NUMEDIT
                       MOVE LENGTH OF VALINDEX-PARM-AREA
                                       TO WSLG-CHANLLEN-NUMEDIT
                       STRING 'EIBCALEN '     WSLG-EIBCALEN-NUMEDIT
                              ' VERSUS '      WSLG-CHANLLEN-NUMEDIT
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                       PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                       GO TO Z9999-RETURN-TO-CALLING-PGM
                   END-IF

               WHEN OTHER
                   SET VALINDEX-RESPONSE-ERROR
                                       TO TRUE
                   MOVE 'E'            TO VALINDEX-RETURN-STATUS-CODE   E
                   MOVE  1             TO VALINDEX-RETURN-MSG-COUNT
                   MOVE 'A05002'       TO VALINDEX-RETURN-MSG-NUM (1)
                   MOVE 'CHANNEL-ID'   TO VALINDEX-RETURN-MSG-FIELD (1)
                   MOVE 'The channel version is invalid          '
                                       TO VALINDEX-RETURN-MSG-TEXT (1)
                   MOVE WSLG-CHNL-VERS-MSG-ID
                                       TO WSLG-MSG-ID
                   MOVE WSLG-CHNL-VERS-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                   MOVE 'N'            TO WSLG-CONSOLE-MSG-IND
                   MOVE SPACES         TO WSLG-DIAGNOSTIC-INFO
                   STRING 'Received version: ' COMM-VERS-RESP-CD
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                   PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                   GO TO Z9999-RETURN-TO-CALLING-PGM
               END-EVALUATE
           ELSE
               SET VALINDEX-RESPONSE-ERROR
                                       TO TRUE
               MOVE 'E'                TO VALINDEX-RETURN-STATUS-CODE
               MOVE  1                 TO VALINDEX-RETURN-MSG-COUNT
               MOVE 'A05003'           TO VALINDEX-RETURN-MSG-NUM (1)
               MOVE 'Channel-Id'       TO VALINDEX-RETURN-MSG-FIELD (1)
               MOVE 'The channel id value is invalid         '
                                       TO VALINDEX-RETURN-MSG-TEXT (1)
               MOVE WSLG-CHNL-ID-MSG-ID
                                       TO WSLG-MSG-ID
               MOVE WSLG-CHNL-ID-MSG-TEXT
                                       TO WSLG-MSG-TEXT
               MOVE 'N'                TO WSLG-CONSOLE-MSG-IND
               MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
               STRING 'Received Channel Id: ' COMM-CHANNEL-ID
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
               PERFORM Z9998-LOG-ERROR-VIA-UIS000W
               GO TO Z9999-RETURN-TO-CALLING-PGM
           END-IF.
      /
       Z9998-LOG-ERROR-VIA-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W TO LOG 2 ERROR MESSAGES TO THE WSLG
      *    TRANSIENT DATA QUEUE AND, IF REQUESTED, TO THE Z/OS CONSOLE.
      *    THE FIRST MESSAGE (SEQ# 0) CONTAINS DESCRIPTIVE ERROR TEXT.
      *    THE SECOND MESSAGE (SEQ# 1) CONTAINS DIAGNOSTIC INFORMATION.
      ******************************************************************

           MOVE '0'                    TO WSLG-MSG-SEQ-NUM.

           IF VALINDEX-RETURN-STATUS-CODE = 'F'
               MOVE 'WGA003'           TO WSLG-PROGRAM-NAME
           ELSE
               MOVE 'WGA003X'          TO WSLG-PROGRAM-NAME
           END-IF.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           MOVE '1'                    TO WSLG-MSG-SEQ-NUM.
           MOVE EIBTASKN               TO WSLG-CICS-TASK-NUMBER.
           MOVE WSLG-DIAGNOSTIC-AREA   TO WSLG-MSG-TEXT.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.


       Z9998-LINK-TO-PROGRAM-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W.  IF THE LINK FAILS,
      *    ABEND THE TASK WITH A 'WCIX' DUMP CODE.
      ******************************************************************

           EXEC CICS LINK
                PROGRAM (WSLG-PROGRAM-UIS000W)
                COMMAREA(WSLG-USING-LIST)
                RESP    (WSLG-CICS-RESP)
                RESP2   (WSLG-CICS-RESP2)
           END-EXEC.

           IF WSLG-CICS-RESP NOT = DFHRESP(NORMAL)
               EXEC CICS ABEND
                    ABCODE('WCIX')
               END-EXEC
           END-IF.
      /
       Z9999-RETURN-TO-CALLING-PGM.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM.
      ******************************************************************

           MOVE VALINDEX-PARM-AREA     TO DFHCOMMAREA (1:LENGTH OF
                VALINDEX-PARM-AREA).

           EXEC CICS RETURN
           END-EXEC.
